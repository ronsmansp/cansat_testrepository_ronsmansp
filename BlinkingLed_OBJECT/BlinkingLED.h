#pragma once
#include "Arduino.h"

//DECLARATION DE LA CLASSE
class BlinkingLED {
  public :
    
    BlinkingLED (byte thePinNumber, unsigned long theDuration);                      //METHODE 1 DE LA CLASSE
  
    void run();                           //METHODE 2 DE LA CLASSE
    
  private :
    uint8_t duration;
    unsigned long ts;
    byte pinNumber;    
} ;
