#include "BlinkingLED.h"

BlinkingLED::BlinkingLED(byte thePinNumber, unsigned long theDuration) {

  duration = theDuration;
  pinNumber = thePinNumber;
  pinMode (pinNumber, OUTPUT);
  ts = millis();

}

void BlinkingLED::run() {
  Serial.println(ts);
  Serial.println ("Duration = ");
  Serial.println(duration);
  //Serial.print =("theDuration = ");
  //Serial.println (theDuration);
  if (( millis() - ts) >= duration ) {
    digitalWrite(pinNumber, !digitalRead(pinNumber));
    ts = millis(); 
  }
}
